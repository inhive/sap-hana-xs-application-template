﻿/*
 * Declaration of all components available in "SAP HANA XS JavaScript APIs"
 * 
 */

declare module $ {
    export var response: IWebResponse;
    export var request: IWebRequest;
    export var db: IDb;
    export var trace: IXsTrace;

}

/*
interface IXsApiStatic {
    response: IWebResponse;
    request: IWebRequest;
    db: IDb;
    "import"(path: string, lib: string): void;
}
    */


interface IXsTrace {
    error(message: any);
    info(message: any);
    fatal(message: any);
    debug(message: any);
    warning(message: any);
}

interface IWebResponse extends IWebEntity {
}

interface IWebRequest extends IWebEntity {
    body: IBody
}

interface IBody {
    asArrayBuffer(): ArrayBuffer;
    asString(): string;
}

interface IEntityList {
    length: number;
    create(): IWebEntity;
}

interface ITupelList {
    length: number;
    get(name: string): string;
    set(name: string, value: any): boolean;
}

interface IDb {
    getConnection(): IConnection;
}

interface IConnection {
    close();
    commit;
    isClosed(): boolean;
    prepareCall(statement: string): ICallableStatement;
    prepareStatement(statement: string): IPreparedStatement;
    rollback();
    setAutoCommit(enable: boolean);
}

interface IDataReader {
    close();
    getBlob(index: number): ArrayBuffer;
    getBString(index: number): ArrayBuffer;
    getClob(index: number): string;
    getDate(index: number): Date;
    getDecimal(index: number): number;
    /**
* Returns a number value of a DOUBLE, FLOAT or REAL parameter
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @returns {number} Number representation
* @throws Throws an error if the index parameter is not valid or the SQL type of the queried parameter does not match.
* @throws {$.db.SQLException}
*/
    getDouble(index: number): number;
    /**
* Returns an integer value of a TINYINT, SMALLINT, INT or BIGINT parameter types
* An exception will be thrown if the value is bigger than 9007199254740992 (2^53) or smaller than -9007199254740992 (-2^53).
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @returns {integer} Integer representation
* @throws Throws an error if the index parameter is not valid or the SQL type of the queried parameter does not match.
* @throws {$.db.SQLException}
*/
    getInteger(index: number): number;
    /**
* Returns the string value of a NCLOB or TEXT parameter
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @returns {string} String representation
* @throws Throws an error if the index parameter is not valid or the SQL type of the queried parameter does not match.
* @throws {$.db.SQLException}
*/
    getNClob(index: number): string;
    /**
* Sets a string parameter used for NCHAR, NVARCHAR parameter types, which should be used for strings containing unicode characters. <br>
* This function converts the given unicode string into a storable format. Make sure you use getNString to read the data.
* If you use getString on a column you wrote with setNString, an exception is thrown if the string contains unicode characters lager than 0xFFFF.
* @param {integer} columnIndex The index of the parameter <b>starting from 1</b>
* @param {string} value The string value to be set for this parameter
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    getNString(index: number): string;
    /**
* Returns a number value of the specified column. getReal is used for REAL column types.
* @param {integer} columnIndex The target column <b>starting from 1</b>
* @returns {number} Number representation
* @throws Throws an error if the index parameter is not valid.
* @throws {$.db.SQLException}
*/
    getReal(index: number): number;
    /**
* Used to retrieve the value of a SECONDDATE parameter
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @returns {Date} A JavaScript date object representing the value
* @throws Throws an error if the index parameter is not valid or the SQL type of the queried parameter does not match.
* @throws {$.db.SQLException}
*/
    getSeconddate(index: number): Date;
    /**
* Sets a string parameter used for NCHAR, NVARCHAR parameter types, which should be used for strings containing unicode characters. <br>
* This function converts the given unicode string into a storable format. Make sure you use getNString to read the data.
* If you use getString on a column you wrote with setNString, an exception is thrown if the string contains unicode characters lager than 0xFFFF.
* @param {integer} columnIndex The index of the parameter <b>starting from 1</b>
* @param {string} value The string value to be set for this parameter
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    getString(index: number): string;
    /**
* Used to retrieve the value of a TIME parameter
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @returns {Date} A JavaScript date object representing the value
* @throws Throws an error if the index parameter is not valid or the SQL type of the queried parameter does not match.
* @throws {$.db.SQLException}
*/
    getTime(index: number): string;
    /**
* Used to retrieve the value of a TIMESTAMP parameter. <br>
* As this type contains only time information and no date, the JavaScript's date object will always be 1 Jan 1970 plus the time offset.<br>
* For example: if the stored value is 10:00:00, the JavaScript date object will specify: 1 Jan 1970 10:00:00
*
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @returns {Date} A JavaScript date object representing the value
* @throws Throws an error if the index parameter is not valid or the SQL type of the queried parameter does not match.
* @throws {$.db.SQLException}
*/
    getTimestamp(index: number): string;
    /**
* Checks if the statement is closed.
* @returns {boolean} Returns true if the statement is already closed, false if not
* @throws Throws an error if the object the method is being called on is not valid. 
* @throws {$.db.SQLException}
*/
    isClosed(): boolean;
}

interface ICallableStatement extends IDataReader {

    execute(): boolean;

    /**
* Checks if more resultsets are available and prepares the next resultset for retrieval
* @returns {boolean} True if the next resultset is available
* @throws Throws an error if the object the method is being called on is not valid. 
* @throws {$.db.SQLException}
*/
    getMoreResults(): boolean;

    /**
* Returns the metadata for this statement
* @returns {db.ParameterMetaData} ParameterMetaData object
* @throws Throws an error if the object the method is being called on is not valid. 
* @throws {$.db.SQLException}
*/
    getParameterMetaData(): IParameterMetaData;

    /**
* Returns a resultset representing a table output parameter
* @returns {db.ResultSet} ResultSet of the next output table parameter
* @throws Throws an error if the object the method is being called on is not valid. 
* @throws {$.db.SQLException}
*/
    getResultSet(): IResultSet;

    /**
* Sets an integer parameter used for BIGINT parameter types
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @param {integer} value The number value to be set for this parameter
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setBigInt(index, value);
    /**
* Sets a Timestamp parameter used for TIMESTAMP parameter types
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @param {Date|string} value The timestamp value to be set for this parameter
* The default format is: <b>date</b> <b>separator</b> <b>time</b>, for example, <br>
* 2001-01-02 01:02:03.123, where <b>date</b> is the format to use for the date value <br>
* (see setDate), <b>separator</b> can be a space, a comma, or the letter T, and <br>
* <b>time</b> is the format to use for the time value (see setTime).<br>
* Examples:<br> 
* 2001-01-02 01:02:03.123<br>
* 2001-01-02,01:02:03.123<br>
* 2001-01-02T01:02:03.123<br>
* <i>st.setTimestamp(4,"01.02.2003 01:02:03.123", "DD.MM.YYYY HH:MI:SS.FF");</i> 
* @param {string} [format=""] Optional, see also setDate and setTime. 
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setTimestamp(columnIndex, value, format);
    /**
* setBlob is used to specify the values for CHAR, VARCHAR, NCHAR, NVARCHAR, BINARY, VARBINARY parameter types.
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @param {ArrayBuffer} value The ArrayBuffer object to be set for this parameter, can also be an array of integers or a string.
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setBlob(index: number, value: any);
    /**
* Sets a string parameter used for BINARY, VARBINARY parameter types. <br> Remark: the BINARY type is deprecated - its behavior in row store and column store differs in that row store may pad with zeros. 
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @param {ArrayBuffer} value The ArrayBuffer object to be set for this parameter.
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setBString(index: number, value: any);
    /**
* setClob is used to specify the values for CLOB parameter types.
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @param {string} value The string value to be set for this parameter
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setClob(index: number, value: any);
    /**
* Sets a Date parameter for DATE parameters, but works with TIME and TIMESTAMP.<br/>
* It is not possible to set the time with setDate; you can only set the date
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @param {Date|string} value The date to be set for this parameter <br/>
*        The parameter can be a Date object, a string in default ptime format (YYYY-MM-DD), or a string in the optionally specified format.<br/>
*        For example: 'yyyymmdd' or 'yyyy-mm-dd' or 'yyyy/mm/dd'
* <ul>
* <li>Y,YY,YYY,YYYY-year</li>
* <li>D-day</li>
* <li>J-julian day</li> 
* <li>MONTH-by name,MON-abbr.</li> 
* <li>M-month</li> 
* <li>Q-quarter</li>
* <li>RM-roman numeral month</li> 
* <li>W-week of month</li> 
* <li>WW-week of year.</li>
* </ul> <br>
* Note that when you construct a new Date JavaScript object, the month number starts from <b>0</b> (not 1).<br>
* For example the following statement represents <em>1st of Jan, 2010</em>:<br>
* <i>new Date(2010,0,1);</i>
*
* @param {string} [format=""] One of the following formats: <br/>
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setDate(index: number, value: any, format: string);
    /**
* setDecimal sets a decimal parameter used for DECIMAL parameter types
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @param {number} value The number value to be set for this parameter
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setDecimal(index: number, value: any);
    /**
* setDouble sets a double parameter used for FLOAT and DOUBLE parameter types
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @param {number} value The number value to be set for this parameter
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setDouble(index: number, value: any);
    /**
* Sets an integer parameter used for TINYINT, SMALLINT, INT parameter types
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @param {integer} value The integer value to be set for this parameter
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setInteger(index: number, value: any);
    /**
* setNClob is used to specify the values for NCLOB parameter types.
* @param {integer}  index The index of the parameter <b>starting from 1</b>
* @param {string} value The string value to be set for this parameter.
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setNClob(index: number, value: any);
    /**
* Sets a string parameter used for NCHAR, NVARCHAR parameter types, which should be used for strings containing unicode characters. <br>
* This function converts the given unicode string into a storable format. Make sure you use getNString to read the data.
* If you use getString on a column you wrote with setNString, an exception is thrown if the string contains unicode characters lager than 0xFFFF.
* @param {integer} columnIndex The index of the parameter <b>starting from 1</b>
* @param {string} value The string value to be set for this parameter
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setNString(index: number, value: any);
    /**
* Sets a null parameter used for all parameter types
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setNull(index: number);
    /**
* setReal sets a real parameter used for REAL parameter types
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @param {number} value The number value to be set for this parameter
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setReal(index: number, value: any);
    /**
* Sets an integer parameter used for SMALLINT parameter types
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @param {integer} value The integer value to be set for this parameter
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setSmallInt(index: number, value: any);
    /**
* Sets a string parameter used for CHAR, VARCHAR column types; ASCII only, not suitable for strings containing Unicode characters <br>
* This function can be used to store strings containing ASCII and a subset of Unicode (namely BMP; the first 0xFFFF characters). <br>
* This function does not convert data; to improve performance, it stores data directly in the database. <br>
* Note that special characters (in Unicode SMP or SIP) can cause the read operation to fail. 
* For more information see {@link http://en.wikipedia.org/wiki/Plane_%28Unicode%29|Plane (Unicode)}. <br>
* If also need special unicode characters or if you are not sure what this means it is safer to use setNString.
* @param {integer} columnIndex The index of the parameter in the statement <b>starting from 1</b>
* @param {string} value The string value to be set for this parameter
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setString(index: number, value: any);
    /**
* Sets a Time parameter used for TIME parameter types (hour, min, sec) - milliseconds(mls) cannot be set 
* @param {integer} index The index of the parameter, <b>starting from 1</b>
* @param {Date|string} value The Date value to be set for this parameter
* <ul>
* <li>HH:MI:SS.FF AM</li>
* <li>HH24:MI:SS.FF</li>
* <li>HH:MI:SS AM</li>
* <li>HH24:MI:SS</li>
* <li>HH:MI AM</li>
* <li>HH24:MI</li>
* <li>HH24:MI:SS.FF Z</li>
* <li>HH24:MI:SS Z</li>
* </ul>
* @param {string} [format=""] One of the following formats:<br/>
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setTime(index: number, value, format);
    /**
* Sets a Timestamp parameter used for TIMESTAMP parameter types
* @param {integer} index The index of the parameter <b>starting from 1</b>
* @param {Date|string} value The timestamp value to be set for this parameter
* The default format is: <b>date</b> <b>separator</b> <b>time</b>, for example, <br>
* 2001-01-02 01:02:03.123, where <b>date</b> is the format to use for the date value <br>
* (see setDate), <b>separator</b> can be a space, a comma, or the letter T, and <br>
* <b>time</b> is the format to use for the time value (see setTime).<br>
* Examples:<br> 
* 2001-01-02 01:02:03.123<br>
* 2001-01-02,01:02:03.123<br>
* 2001-01-02T01:02:03.123<br>
* <i>st.setTimestamp(4,"01.02.2003 01:02:03.123", "DD.MM.YYYY HH:MI:SS.FF");</i> 
* @param {string} [format=""] Optional, see also setDate and setTime. 
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setTimestamp(index: number, value, format);
    /**
* Sets an integer parameter used for TINYINT parameter types
* @param {integer} index The index of the parameter, <b>starting from 1</b>
* @param {integer} value The integer value to be set for this parameter (unsigned char: min 0, max 255)
* @throws Throws an error on invalid parameters.
* @throws {$.db.SQLException}
*/
    setTinyInt(index: number, value: number);

}

interface IPreparedStatement extends ICallableStatement {
    /**
     * Adds last parameter values and iterates to the next batch slot
     * @throws Throws an error if the statement if setBatchSize has not been called successfully or if the object the method is being called on is not valid. 
     * @throws {$.db.SQLException}
     */
    addBatch();
    /**
 * Executes a common statement
 * @returns {boolean} True if the statement was a SELECT statement and false if the statement was INSERT, UPDATE, or DELETE
 * @throws Throws an error if the object the method is being called on is not valid. 
 * @throws {$.db.SQLException}
 */
    execute(): boolean;
    /**
 * Executes a batch insertion. Use setBatchSize and addBatch to prepare for batch execution.
 * @returns {array} Array with integers representing the number of updated rows per batch
 * @throws Throws an error if the object the method is being called on is not valid. 
 * @throws {$.db.SQLException}
 */
    executeBatch(): Int32Array;
    /**
 * Executes an SQL statement
 * @returns {db.ResultSet} ResultSet Holds the result of the executed SQL statement
 * @throws Throws an error if the object the method is being called on is not valid. 
 * @throws {$.db.SQLException}
 */
    executeQuery(): IResultSet;
    /**
 * Executes an update statement
 * @returns {integer} The number of changed rows resulting from the update statement
 * @throws Throws an error if the object the method is being called on is not valid. 
 * @throws {$.db.SQLException}
 */
    executeUpdate(): number;
    /**
 * Checks if more resultsets are available and prepares the next resultset for retrieval
 * @returns {boolean} True if the next resultset is available
 * @throws Throws an error if the object the method is being called on is not valid. 
 * @throws {$.db.SQLException}
 */
    getMoreResults(): boolean;
    /**
 * Returns the metadata of the prepared statement
 * @returns {db.ParameterMetaData} ParameterMetaData object
 * @throws Throws an error if the object the method is being called on is not valid. 
 * @throws {$.db.SQLException}
 */
    getParameterMetaData(): IParameterMetaData;
    /**
 * Returns a resultset representing a table output parameter
 * @returns {db.ResultSet} ResultSet of the next output table parameter
 * @throws Throws an error if the object the method is being called on is not valid. 
 * @throws {$.db.SQLException}
 */
    getResultSet(): IResultSet;
    /**
* Checks if the statement is closed.
* @returns {boolean} Returns true if the statement is already closed, false if not
* @throws Throws an error if the object the method is being called on is not valid. 
* @throws {$.db.SQLException}
*/
    isClosed(): boolean;
    /**
 * Reserves space for batch insertion
 * @param {integer} size The number (count) of batch insertions that will be performed
 * @throws Throws an error on invalid parameters or if the object the method is being called on is not valid. 
 * @throws {$.db.SQLException}
 */
    setBatchSize(size);

    /**
     * setText is used to specify the values for TEXT column types.
     * @param {integer} columnIndex The index of the parameter in the prepared statement <b>starting from 1</b>
     * @param {string} value The string value to be set for this parameter
     * @throws Throws an error on invalid parameters.
     * @throws {$.db.SQLException}
     */
    setText(index: number, value: string);
}

interface IParameterMetaData {

}

interface IResultSet extends IDataReader {
    /**
	* Closes the ResultSet
	* @throws Throws an error if the object the method is being called on is not valid. 
	* @throws {$.db.SQLException}
	*/
    next(): boolean;
    /**
 * Returns the metadata of the ResultSet
 * @returns {db.ResultSetMetaData} ResultSetMetaData object
 * @deprecated Use getMetaData on ResultSet.
 * @see $.db.ResultSet#getMetaData 
 */
    getMetaData(): IResultSetMetaData;

}

interface IResultSetMetaData {

}

interface IParameterMetaData {

}

interface IWebEntity {

    /**
     * The content type of the entity
     * @type string
     * 
     */
    contentType: string;
    /**
     * The headers of the entity
     * @type $.web.TupelList
     */
    entities: IEntityList;
    /**
     * The headers of the entity
     * @type $.web.TupelList
     */
    headers: ITupelList;
    /**
     * The parameters of the entity
     * @type $.web.TupelList
     */
    parameters: ITupelList;
    /**
     * Sets the body of the entity; 
     * the method supports all elemental JavaScript types and ArrayBuffers.
     * 
     * @param {any|ArrayBuffer} body Can be any elemental JavaScript type or an ArrayBuffer.
     * @returns {undefined}
     * @throws Throws an error if the given parameters can not be used as body
     */
    setBody(body: string);
    /**
 * The HTTP method of the incoming HTTP request
 * @type $.net.http
 */
    method: $.net.http;
    /**
 * The HTTP status code of the outgoing HTTP response
 * @type $.net.http
 */
    status: $.net.http;
    body: IBody;
}

declare module $.net {
    enum http {
        /**
* HTTP Method OPTIONS 
* @type {number}
* @constant
*/
        OPTIONS = 0,
        /**
            * HTTP Method GET 
            * @type {number}
            * @constant
            */
        GET = 1,
        /**
            * HTTP Method HEAD 
            * @type {number}
            * @constant
            */
        HEAD = 2,
        /**
            * HTTP Method POST 
            * @type {number}
            * @constant
            */
        POST = 3,
        /**
            * HTTP Method PUT 
            * @type {number}
            * @constant
            */
        PUT = 4,
        /**
            * HTTP Method DEL 
            * @type {number}
            * @constant
            */
        DEL = 5,
        /**
            * HTTP Method TRACE 
            * @type {number}
            * @constant
            */
        TRACE = 6,
        /**
            * HTTP Method PATCH
            * @type {number}
            * @constant
            */
        CONNECT = 7,
        /**
            * HTTP Method PATCH
            * @type {number}
            * @constant
            */
        PATCH = 8,
        /**
            * @type {number}
            * @constant
            */
        CONTINUE = 100,
        /**
            * @type {number}
            * @constant
            */
        SWITCH_PROTOCOL = 101,
        /**
            * @type {number}
            * @constant
            */
        OK = 200,
        /**
            * @type {number}
            * @constant
            */
        CREATED = 201,
        /**
            * @type {number}
            * @constant
            */
        ACCEPTED = 202,
        /**
            * @type {number}
            * @constant
            */
        NON_AUTHORITATIVE = 203,
        /**
            * @type {number}
            * @constant
            */
        NO_CONTENT = 204,
        /**
            * @type {number}
            * @constant
            */
        RESET_CONTENT = 205,
        /**
            * @type {number}
            * @constant
            */
        PARTIAL_CONTENT = 206,
        /**
            * @type {number}
            * @constant
            */
        MULTIPLE_CHOICES = 300,
        /**
            * @type {number}
            * @constant
            */
        MOVED_PERMANENTLY = 301,
        /**
            * @type {number}
            * @constant
            */
        FOUND = 302,
        /**
            * @type {number}
            * @constant
            */
        SEE_OTHER = 303,
        /**
            * @type {number}
            * @constant
            */
        NOT_MODIFIED = 304,
        /**
            * @type {number}
            * @constant
            */
        USE_PROXY = 305,
        /**
            * @type {number}
            * @constant
            */
        TEMPORARY_REDIRECT = 307,
        /**
            * @type {number}
            * @constant
            */
        BAD_REQUEST = 400,
        /**
            * @type {number}
            * @constant
            */
        UNAUTHORIZED = 401,
        /**
            * @type {number}
            * @constant
            */
        PAYMENT_REQUIRED = 402,
        /**
            * @type {number}
            * @constant
            */
        FORBIDDEN = 403,
        /**
            * @type {number}
            * @constant
            */
        NOT_FOUND = 404,
        /**
            * @type {number}
            * @constant
            */
        METHOD_NOT_ALLOWED = 405,
        /**
            * @type {number}
            * @constant
            */
        NOT_ACCEPTABLE = 406,
        /**
            * @type {number}
            * @constant
            */
        PROXY_AUTH_REQUIRED = 407,
        /**
            * @type {number}
            * @constant
            */
        REQUEST_TIMEOUT = 408,
        /**
            * @type {number}
            * @constant
            */
        CONFLICT = 409,
        /**
            * @type {number}
            * @constant
            */
        GONE = 410,
        /**
            * @type {number}
            * @constant
            */
        LENGTH_REQUIRED = 411,
        /**
            * @type {number}
            * @constant
            */
        PRECONDITION_FAILED = 412,
        /**
            * @type {number}
            * @constant
            */
        REQUEST_ENTITY_TOO_LARGE = 413,
        /**
            * @type {number}
            * @constant
            */
        REQUEST_URI_TOO_LONG = 414,
        /**
            * @type {number}
            * @constant
            */
        UNSUPPORTED_MEDIA_TYPE = 415,
        /**
            * @type {number}
            * @constant
            */
        REQUESTED_RANGE_NOT_SATISFIABLE = 416,
        /**
            * @type {number}
            * @constant
            */
        EXPECTATION_FAILED = 417,
        /**
            * @type {number}
            * @constant
            */
        INTERNAL_SERVER_ERROR = 500,
        /**
            * @type {number}
            * @constant
            */
        NOT_YET_IMPLEMENTED = 501,
        /**
            * @type {number}
            * @constant
            */
        BAD_GATEWAY = 502,
        /**
            * @type {number}
            * @constant
            */
        SERVICE_UNAVAILABLE = 503,
        /**
            * @type {number}
            * @constant
            */
        GATEWAY_TIMEOUT = 504,
        /**
            * @type {number}
            * @constant
            */
        HTTP_VERSION_NOT_SUPPORTED = 505,
    }
}
