﻿/*
 * Declaration of all components available in "SAP HANA XS JavaScript APIs"
 * 
 */

declare module $ {

    export var hdb: IHdb;

    module security {
        module crypto {
            /**
            * Computes MD5 hash or HMAC-MD5 
            * @param {String | ArrayBuffer} data String or binary data to produce the hash from. If the parameter is of type String, its UTF-8 representation will be considered
            * @param {String} [key] Key used for HMAC-MD5 generation
            * @return ArrayBuffer
            * @throws Throws an error if the input parameters are invalid 
            */
            export function md5(data, key): ArrayBuffer;
            /**
            * Computes SHA1 or HMAC-SHA1  
            * @param {String | ArrayBuffer} data String or binary data to produce the hash from. If the parameter is of type String, its UTF-8 representation will be considered
            * @param {String} [key] Key used for HMAC-SHA1 generation
            * @return ArrayBuffer
            * @throws Throws an error if the input parameters are invalid 
            */
            export function sha1(data, key): ArrayBuffer;
            /**
            * Computes SHA256 or HMAC-SHA256  
            * @param {String | ArrayBuffer} data String or binary data to produce the hash from. If the parameter is of type String, its UTF-8 representation will be considered
            * @param {String} [key] Key used for HMAC-SHA256 generation
            * @return ArrayBuffer
            * @throws Throws an error if the input parameters are invalid 
            */
            export function sha256(data, key): ArrayBuffer;

        }

        class Store {
            constructor(secureStoreFile);


            /**
            * read values, accessible by all users of the application 
            * @param {$.security.Store~ReadParameters} parameters parameter object
            * @return String The decrypted value 
            * @throws Throws an error if the access fails or the parameters are invalid 
            */
            read(parameters): string;
            /**
            * read values on user isolation level  
            * @param {$.security.Store~ReadParameters} parameters parameter object
            * @return String The decrypted value 
            * @throws Throws an error if the access fails or the parameters are invalid 
            */
            readForUser(parameters): string;
            /**
            * remove values on application isolation level 
            * @param {$.security.Store~DeletionParameters} parameters 
            * @throws Throws an error if the access or the parameters are invalid 
            */
            remove(parameters);
            /**
            * remove values on user isolation level  
            * @param {$.security.Store~DeletionParameters} parameters 
            * @throws Throws an error if the access or the parameters are invalid 
            */
            removeForUser(parameters);
            /**
            * read values on user isolation level   
            * @param {$.security.Store~WriteParameters} parameters 
            * @throws Throws an error if the access or the parameters are invalid 
            */
            store(parameters);
            /**
            * stores user specific values, no other user can decrypt it   
            * @param {$.security.Store~WriteParameters} parameters 
            * @throws Throws an error if the access or the parameters are invalid 
            */
            storeForUser(parameters);
        }
    }

    module util {
        class SAXParser {

        }

        class Zip {
            constructor(source, index, password);
        }
    }

    module jobs {
        class Job {
            constructor(parameter);
        }
    }
}

interface IHdb {
    getConnection(): IHdbConnection;
}

interface IHdbConnection {
    /**
 * Closes the connection.
 * @throws Throws an error if the operation fails.
 */
    close();
    /**
     * Commits the changes and ends the current transaction. <b>By default autocommit mode is disabled, which means all database changes must be explicitly commited</b>.
     * @throws Throws an error if the object the method is being called on is not valid. 
     */
    commit();
    /**
     * @param {String} queryString The query string to be executed.
     * @param {VarArgs} arguments Variable number of arguments to be bound to the query parameters.
     * @returns $.hdb.ResultSet
     * @throws Throws an error if the statement cannot be executed.
     */
    executeQuery(queryString, arguments): IHdbResultSet;
    /**
    * @param {String} dmlString The DML operation to be executed.
    * @param {VarArgs} arguments Variable number of arguments to be bound to the query parameters.
    * @returns {Number | Array}  Number of affected rows | Array of integers in case of batch update. 
    */
    executeUpdate(dmlString, arguments);
    /**
    * Creates a javascript representing a stored procedure.
    * @param {String} schema The schema to which the procedure belongs.
    * @param {String} procedure The name of the procedure.
    * @returns {Function} 
    */
    loadProcedure(schema, procedure): IHdbProcedureResult;
    /**
     * Reverts the changes and ends the current transaction.
     * @throws Throws an error if the statement cannot be executed.
     */
    rollback();
    /**
    * Changes the auto-commit flag of the connection.
    * @param {Integer} An integer value, which can be either 0 (false) or 1 (true)
    */
    setAutoCommit(enable);
}

interface IHdbResultSetIterator {
    /**
    * Checks if the underlying $.hdb.ResultSet has more rows and sets the value of the iterator to the next row if it exists.
    * @Return {Boolean} True if the result set has more rows otherwise returns false.
    */
    next(): boolean;
    /**
    * Returns the current row that the iterator's value is set to. Should always be called after a call to next();
    * @Return {Object} An object representing the row of a $.hdb.ResultSet. 
    * @throw An object representing the row of a $.hdb.ResultSet. 
    */
    value(): Object;
}

interface IHdbResultSet {
    /**
  * The number of rows in the $.hdb.ResultSet object 
  * @type Number
  */
    length: Number,
	/**
	* Returns an iterator over this result set 
	* @Return {$.hdb.ResultSetIterator} ResultSetIterator
	*/
    getIterator(): IHdbResultSetIterator;
}

interface IHdbProcedureResult{
    (...args: any[]): Array<IHdbResultSet>;
    $resultSets: Array<IHdbResultSet>;
}


declare function describe(description: string, specDefinitions: () => void): void;
declare function xdescribe(description: string, specDefinitions: () => void): void;
declare function it(description: string, func: () => void): void;
declare function xit(description: string, func: () => void): void;
declare function expect(actual: any): jasmine.Matchers;
declare function beforeEach(afterEachFunction: () => void): void;
declare function afterEach(afterEachFunction: () => void): void;
declare function spyOn(obj, methodName: string, ignoreMethodDoesntExist?: boolean): jasmine.Spy;
declare function beforeOnce(beforeOnceFunction: () => void): void;
declare function afterOnce(afterOnceFunction: () => void): void;

declare function runs(func: () => void): void;
declare function waitsFor(latchFunction: () => void, optional_timeoutMessage?: string, optional_timeout?: number): void;
declare function waits(timeout: number): void;

declare module jasmine {
    function any(clazz: any);

    function createSpy(name: string): any;

    function createSpyObj(baseName: string, methodNames: any[]): any;

    interface Matchers {
        toBe(expected): boolean;
        toBeCloseTo(expected: number, precision: number): boolean;
        toBeDefined(): boolean;
        toBeFalsy(): boolean;
        toBeGreaterThan(expected): boolean;
        toBeLessThan(expected): boolean;
        toBeNull(): boolean;
        toBeTruthy(): boolean;
        toBeUndefined(): boolean;
        toContain(expected): boolean;
        toEqual(expected): boolean;
        toHaveBeenCalled();
        toHaveBeenCalledWith(...params: any[]): boolean;
        toMatch(expected): boolean;
        toThrow(expected: string): boolean;
        not: Matchers; // dynamically added in jasmine code
        //Deprecated: toNotBe(expected);  toNotContain(expected) toNotEqual(expected) toNotMatch(expected) wasNotCalled() wasNotCalledWith(
    }

    interface Spy {
        andReturn(value): void;
        andCallThrough(): void;
        andCallFake(fakeFunc: Function): void;

        identity: string;
        calls: any[];
        mostRecentCall: { args: any[]; };
        argsForCall: any[];
        wasCalled: boolean;
        callCount: number;
    }

    interface Clock {
        useMock(): void;
        uninstallMock(): void;
        tick(millis: number): void;
    }

    var Clock: Clock;
}